package com.zuitt.discussion.config;

import com.zuitt.discussion.models.User;
import com.zuitt.discussion.repositories.UserRepository;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

@Component
public class JwtToken implements Serializable {
// to get token from the application.properties

    @Value("${jwt.secret}")
    private String secret;

    @Autowired
    UserRepository userRepository;

    private static final long serialVersionUID = 3975308520860034891L;

//    Time duration of token use (in seconds)
//    5 hours
    public static final long JWT_TOKEN_VALIDITY = 5 * 60 * 60;

    //    This compound method generates the jwt token
    // Jwts.builder() creates a new JWT builder instance. This object is used to build and sign the JWT.
// .setClaims includes the information to show the recipient which is the username// .setSubject adds information about the subject. (The subject username.)
// .setIssuedAt sets the time and date when the token was created
// .setExpiration sets the expiration of the token
// .signWith creates the token using a declared algorithm, with the secret keyword.
// "HS512" is a secure cryptographic algorithm.
// The "secret key" is passed as a parameter and is used to verify the signature later on.
// .compact() is used to generate the final JWT string by compacting the JWT builder object.
    private String doGenerateToken(Map<String, Object> claims, String  subject) {
        return Jwts.builder()
                .setClaims(claims)
                .setSubject(subject)
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + JWT_TOKEN_VALIDITY * 1000))
                .signWith(SignatureAlgorithm.HS512, secret)
                .compact();
    }

    public String generateToken(UserDetails userDetails) {
        Map<String, Object> claims = new HashMap<>();
// Retrieves user information form the userRepository by ising the username from the UserDetails object
        User user = userRepository.findByUsername(userDetails.getUsername());
//        id is added to the claims of JWT to be able to be used to identify the user in the server.
        claims.put("user", user.getId());

        return doGenerateToken(claims, user.getUsername());


    }
//        token validation
//    returns a bool value to check if the token belongs to the user and if the token is not expired
   public Boolean validateToken(String token, UserDetails userDetails) {
            final String username = getUsernameFromToken(token);

            return (username.equals(userDetails.getUsername()) && isTokenExpired(token));
    }
//    functional interface
    public <T> T getClaimFromToken(String token, Function<Claims, T> claimsResolver) {
//        returns a generic type
        final Claims claims = getAllClaimsFromToken(token);

        return claimsResolver.apply(claims);
    }

//    This extracts all the claims from the token.
    private Claims getAllClaimsFromToken(String token) {
        return Jwts.parser()
                .setSigningKey(secret)
                .parseClaimsJws(token)
//                this will contain all the claims that were inserted in the JWT
                .getBody();
    }

    public String getUsernameFromToken(String token) {
        String claim = getClaimFromToken(token, Claims::getSubject);

        return claim;
    }

    public Date getExpirationDateFromToken(String token) {
        return getClaimFromToken(token, Claims::getExpiration);
    }

//    used to check if the JWT token has expired
    private Boolean isTokenExpired(String token) {
        final Date expiration = getExpirationDateFromToken(token);

        return expiration.before(new Date());
    }







}