package com.zuitt.discussion.services;


import com.zuitt.discussion.config.JwtToken;
import com.zuitt.discussion.models.Post;
import com.zuitt.discussion.models.User;
import com.zuitt.discussion.repositories.PostRepository;
import com.zuitt.discussion.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class PostServiceImpl implements PostService {

//  Makes u directly access the PostRepository
//  Allows us to use the methods and create an instance of the Post Repository.

    @Autowired
    private PostRepository postRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    JwtToken jwtToken;

//    Method for creating post
    public void createPost(String stringToken, Post post) {
//      save method will save the post to the database
        //Retrieve the "User" object using the extracted username from the JWT Token
        User author = userRepository.findByUsername(jwtToken.getUsernameFromToken(stringToken));

        Post newPost = new Post();
        newPost.setTitle(post.getTitle());
        newPost.setContent(post.getContent());
        newPost.setUser(author);
        postRepository.save(newPost);
        postRepository.save(post);
    }

//    Get All posts.
//    Retrieves a series of records within the table
    public Iterable<Post> getPosts() {
//        using method findAll() to get all records from the table
        return postRepository.findAll();
    }

//    Delete post by id
    public ResponseEntity deletePost(Long id) {
        postRepository.deleteById(id);
        return new ResponseEntity("Post deleted successfully", HttpStatus.OK);
    }

    public ResponseEntity updatePost(Long id, Post post) {
        Post postForUpdate = postRepository.findById(id).get();

        postForUpdate.setTitle((post.getTitle()));
        postForUpdate.setContent((post.getContent()));

        postRepository.save(postForUpdate);

        return new ResponseEntity<>("Post updated successfully", HttpStatus.OK);

    }




}
