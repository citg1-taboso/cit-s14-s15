package com.zuitt.discussion.repositories;

import com.zuitt.discussion.models.Post;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

// makes it accessible within the java program/application
// These contains method for database manuipulation

@Repository
public interface PostRepository extends CrudRepository<Post, Object> {

}